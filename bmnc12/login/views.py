from django.shortcuts import render
from core.models import Pengguna
from django.db import connection,transaction
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.hashers import *

def index(request):
    if 'bmnc_user_login' in request.session.keys():
        return render(request, 'login/halaman-masuk-berhasil.html', {})
    response = {}
    response["render_registrasi"] = True
    return render(request, 'login/halaman-masuk.html', response)
    
def otorisasi_login(request):
    print ("#### Mencoba login")

    if request.method == "POST":
        username = request.POST['username'].strip()
        password = request.POST['password']
        
        print ("#### Login atas nama", username)
        
        id = None
        adalah_narasumber = False
        
        with connection.cursor() as cursor:
            cursor.execute("SELECT password, id FROM PENGGUNA WHERE username = %s", [username])
            row = cursor.fetchone()
            if row is None:
                print ("#### Login gagal, username mungkin tidak ada")
                messages.error(request, "Maaf, username atau password salah")
                return HttpResponseRedirect(reverse("login:index"))
            elif not check_password(password, row[0]):
                print ("#### Login gagal, username mungkin ada tapi pass salah")
                messages.error(request, "Maaf, username atau password salah")
                return HttpResponseRedirect(reverse("login:index"))
            else:
                id = row[1]                
                cursor.execute("SELECT * FROM NARASUMBER WHERE id = %s", [id])
                if cursor.fetchone() is not None:
                    adalah_narasumber = True
        request.session['adalah_narasumber'] = adalah_narasumber
        request.session['bmnc_user_login'] = username
        
        return render(request, 'login/halaman-masuk-berhasil.html', {})
    return HttpResponseRedirect(reverse('login:index'))

def otorisasi_logout(request):
    request.session.flush() # Menghapus semua session
    return HttpResponseRedirect(reverse('beranda:index'))