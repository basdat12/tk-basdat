from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('otologin', views.otorisasi_login, name='otologin'),
    path('otologout', views.otorisasi_logout, name='otologout'),
]