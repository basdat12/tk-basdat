from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.hashers import *
from django.db import connection,transaction
import datetime

def index(request):
    response = {}
    if 'bmnc_user_login' in request.session.keys():
        response["masuk"] = True
        response["adalah_narasumber"] = request.session["adalah_narasumber"]
        if response["adalah_narasumber"]:
            return render(request, 'buat_berita/buat_berita.html', response)
        else:
            messages.error(request, "Maaf,anda bukan Narasumber")
            return HttpResponseRedirect(reverse("beranda:index"))
    else:
        return HttpResponseRedirect(reverse("login:index"))
        
def buat_berita(request):
    if(request.method == 'POST'):
        req = request.POST
        url = str(req["Url"])
        now = datetime.datetime.now()

        #cek panjang dari char setiap isi
        if(len(req["Judul"])>100):
            messages.error(request, "Judul Max 100")
            return HttpResponseRedirect(reverse("buat_berita:index"))
        elif(len(req["Url"])>50):
            messages.error(request, "Url Max 50")
            return HttpResponseRedirect(reverse("buat_berita:index"))
        elif(len(req["Topik"])>100):
            messages.error(request, "Topik Max 100")
            return HttpResponseRedirect(reverse("buat_berita:index"))


        #cek apakah url sudah ada atau tidak
        with connection.cursor() as cursor:
            cursor.execute("SELECT url FROM BERITA WHERE url = %s",[url]);
            if cursor.fetchone() is not None:
                messages.error(request, "Maaf, URL sudah ada")
                return HttpResponseRedirect(reverse("buat_berita:index"))

        #cek apakah jumlah kata sudah benar
        try:
            angka = int(req["Jumlah Kata"])
            rating = 0
        except:
            messages.error(request, "Maaf, jumlah kata harus int")
            return HttpResponseRedirect(reverse("buat_berita:index"))


        date = (now.strftime("%Y/%m/%d"))
        judul = req["Judul"].strip()
        topik =  req["Topik"].strip()

        #jika url tidak terduplicate
        with connection.cursor() as cursor:
            #ambil universitas
            name = (request.session.get("bmnc_user_login", default=None))
            cursor.execute('SELECT id FROM PENGGUNA where username = %s',[name]);
            id_pengguna = cursor.fetchone()
            id_user = str(id_pengguna[0])
            tag = (req["Tag"]);
            cursor.execute("SELECT id_universitas FROM NARASUMBER where id = %s",[id_user]);
            id_ui = cursor.fetchone()
            cursor.execute("INSERT INTO BERITA(url, judul, topik, created_at, updated_at, jumlah_kata, rerata_rating, id_universitas) "
                           + "VALUES (%s, %s, %s, %s, %s, %s, %s, %s);", [url, judul, topik, date, date, angka,rating,str(id_ui[0])]);

            cursor.execute("INSERT INTO NARASUMBER_BERITA(id_narasumber,url_berita) "
                           + "VALUES (%s,%s);", [str(id_user),url]);

            cursor.execute("INSERT INTO TAG(url_berita,tag) "
                           + "VALUES (%s,%s);", [url,tag]);


        return HttpResponseRedirect(reverse("beranda:index"))
