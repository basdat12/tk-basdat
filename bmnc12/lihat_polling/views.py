from django.shortcuts import render
from django.db import connection,transaction
from django.http import HttpResponseRedirect
from django.urls import reverse

def index(request):
    response = {}
    response.update(gather_data())
    
    if 'bmnc_user_login' in request.session.keys():
        response["masuk"] = True
        response["adalah_narasumber"] = request.session["adalah_narasumber"]
    return render(request, 'lihat-polling.html', response)


def gather_data():
### what do i need:
# polling_start
# polling_end
#
## respon
# jawaban
# jumlah dipilih

    result = {}
    polling_biasa_data = get_db_data("select id_polling, url, deskripsi "
                                     "from polling_biasa")

    if (len(polling_biasa_data) == 0):
        return {}

    #  print(polling_biasa_data)

    for poll in polling_biasa_data:
        polling_data = get_db_data("select p.id, p.polling_start, p.polling_end "
                                   "from polling p, polling_biasa pb "
                                   "where p.id = %s", [poll["id_polling"]])

        poll.update(polling_data[0])

        #  print("polling data", polling_data)
        #  print(poll)

        respon_data = get_db_data("select jawaban, jumlah_dipilih "
                                  "from respon "
                                  "where id_polling = %s", [poll["id_polling"]])
        print(poll["id_polling"])
        print(respon_data)

        poll.update({"respons": respon_data})
        #  print("respon data", respon_data)
        #  print(poll)

    print(polling_biasa_data)
    result = polling_biasa_data

    return {"polls": result}


def get_db_data(query, param=[]):
    with connection.cursor() as cursor:
        cursor.execute(query, param)
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
