from django.shortcuts import render

def index(request):
    response = {}
    if "bmnc_user_login" in request.session.keys():
        response["masuk"] = True
        response["adalah_narasumber"] = request.session["adalah_narasumber"]
    else:
        response["render_registrasi"] = True
        response["render_masuk"] = True
    return render(request, 'beranda/halaman-utama.html', response)