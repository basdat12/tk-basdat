from django.shortcuts import render
from core.models import Pengguna
from django.db import connection,transaction
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.hashers import *

def index(request):
    if 'bmnc_user_login' in request.session.keys():
        username = request.session['bmnc_user_login']
        response = {'username': username}
        response.update(gather_data(username))
        
        response["masuk"] = True
        if 'adalah_narasumber' in request.session.keys():
            response["adalah_narasumber"] = request.session["adalah_narasumber"]
        
        print("the response is ", response)
        return render(request, 'personal-profile.html', response)
    else:
        return HttpResponseRedirect(reverse("login:index"))


def gather_data(username):
# what do i need:
## pengguna
# username
## narasumber
# name
# email
# tempat_lahir
# tanggal_lahir
# nomer_hp

## dosen/staff/mahasiswa
# jabatan
# npm
# status_kemahasiswaan
    result = {}

    with connection.cursor() as cursor:
        cursor.execute("select n.nama, n.email, n.tempat, n.tanggal, n.no_hp "
                       "from narasumber n, pengguna p "
                       "where p.username=%s "
                       "and n.id = p.id", [username])

        columns = [col[0] for col in cursor.description]
        result.update([
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ][0])

    mahasiswa_data = get_db_data("select m.npm, m.status "
                             "from mahasiswa m, narasumber n, pengguna p "
                             "where p.username = %s "
                             "and n.id = p.id "
                             "and n.id = m.id_narasumber"
                             , [username])

    dosen_data = get_db_data("select d.nik_dosen, d.jurusan "
                             "from dosen d, narasumber n, pengguna p "
                             "where p.username = %s "
                             "and n.id = p.id "
                             "and n.id = d.id_narasumber"
                             , [username])

    staf_data = get_db_data("select s.nik_staf, s.posisi "
                             "from staf s, narasumber n, pengguna p "
                             "where p.username = %s "
                             "and n.id = p.id "
                             "and n.id = s.id_narasumber"
                             , [username])
    if len(mahasiswa_data) > 0:
        result.update(mahasiswa_data[0])
        result.update({"jabatan": "Mahasiswa"})
    if len(dosen_data) > 0:
        result.update(dosen_data[0])
        result.update({"jabatan": "Dosen"})
    if len(staf_data) > 0:
        result.update(staf_data[0])
        result.update({"jabatan": "Staf"})

    if len(result) == 0:
        return HttpResponseRedirect(reverse("login:index"))
    else:
        print(result)
        return result

def get_db_data(query, param):
    with connection.cursor() as cursor:
        cursor.execute(query, param)
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
