from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('registrasi', views.registrasi, name='registrasi'),
    path('cek-noid-ada', views.cek_nomor_identitas_ada_ajax, name='cek_noid_ada'),
    path('cek-username-ada', views.cek_username_ada_ajax, name='cek_username_ada'),
]