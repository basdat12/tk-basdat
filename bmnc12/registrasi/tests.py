from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

class RegistrationUnitTest(TestCase):

    def test_home_page_is_exist(self):
        response = self.client.get('/registrasi/')
        self.assertEqual(response.status_code, 200)

    def test_using_index_func(self):
        found = resolve('/registrasi/')
        self.assertEqual(found.func, index)