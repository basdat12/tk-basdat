from django.shortcuts import render
from django.urls import reverse
from core.models import Universitas, Narasumber, Pengguna
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib import messages
from django.contrib.auth.hashers import *
from django.views.decorators.csrf import csrf_exempt
import json, re, datetime

from django.db import connection,transaction

listUniv = Universitas.objects.raw("SELECT id FROM UNIVERSITAS");
response = {"daftar_univ": listUniv}
kepanjangan_jabatan = {"M": "MAHASISWA", "D":"DOSEN", "S":"STAF"}
elemen_form = ('nama', 'jabatan', 'username', 'nomor-identitas',
    'tempat-lahir', 'tanggal-lahir', 'email',  'status', 'id-universitas');

def index(request):
    response["jumlah_narasumber"] = 0
    with connection.cursor() as cursor:
        cursor.execute("SELECT id FROM NARASUMBER")
        row = cursor.fetchall()
        if row is not None:
            response["jumlah_narasumber"] = len(row)
            
    if "bmnc_user_login" not in request.session.keys():
        response["render_masuk"] = True
    return render(request, 'registrasi/halaman-registrasi.html', response)
    
def registrasi(request):
    if(request.method == 'POST'):
        req = request.POST
        
        # Validasi semua nilai yang dibutuhkan terisi
        
        hasil_cek = cek_setiap_key_ada(req)
        
        # Validasi password
        
        if hasil_cek is not None:
            messages.error(request, "Maaf, semua field selain nomor HP harus diisi")
            return HttpResponseRedirect(reverse("registrasi:index"))
        
        # Validasi username
        
        username = req["username"].strip()
        password = req["password"]
        no_id    = req["nomor-identitas"].strip()
        
        if cek_username_ada(username):
            messages.error(request, "Maaf, username sudah ada")
            return HttpResponseRedirect(reverse("registrasi:index"))
        
        if not cek_username_valid(username):
            messages.error(request, "Maaf, username tidak valid")
            return HttpResponseRedirect(reverse("registrasi:index"))
    
        if not cek_password_valid(password):
            messages.error(request, "Maaf, password minimal 8 karakter")
            return HttpResponseRedirect(reverse("registrasi:index"))
        
        # Validasi nomor identitas
        
        if cek_nomor_identitas_ada(no_id, kepanjangan_jabatan[req["jabatan"]]):
            messages.error(request, "Maaf, nomor identitas sudah ada")
            return HttpResponseRedirect(reverse("registrasi:index"))
        
        if not cek_nomor_identitas_valid(no_id):
            messages.error(request, "Maaf, nomor identitas harus digit")
            return HttpResponseRedirect(reverse("registrasi:index"))
            
        # Validasi email
        
        if not cek_email_valid(req['email']):
            messages.error(request, "Maaf, email tidak valid")
            return HttpResponseRedirect(reverse("registrasi:index"))
        
        # Validasi tanggal
        
        if not cek_tanggal_valid(req['tanggal-lahir']):
            messages.error(request, "Maaf, tanggal lahir tidak valid")
            return HttpResponseRedirect(reverse("registrasi:index"))
        
        
        # Menentukan id narasumber baru untuk user ini

        i_user = 0
        for i in Pengguna.objects.raw("SELECT id FROM PENGGUNA"):
            i_user = i.id       
        
        i_user += 1
        password = req["password"]
        
        with connection.cursor() as cursor:
        
            # Memasukkan ke database pengguna
            
            cursor.execute("INSERT INTO PENGGUNA(id, username, role, password) "
            + "VALUES (%s, %s, %s, %s);", [i_user, username, "Narasumber", make_password(password)]);
            
            # Memasukkan ke database narasumber
            
            cursor.execute("INSERT INTO NARASUMBER(id,nama,email,tempat,tanggal,no_hp,"
            + "jumlah_berita,rerata_kata,id_universitas) VALUES (%s,%s,%s,%s,%s,%s,0,0,%s)",
            [i_user, req["nama"].strip(), req["email"].strip(), req["tempat-lahir"].strip(),
            req["tanggal-lahir"].strip(), req["no-hp"].strip(), req["id-universitas"].strip()]);
            
            # Memasukkan ke jabatan yang sesuai
            
            if req["jabatan"] == "M":
                cursor.execute("INSERT INTO MAHASISWA(id_narasumber,npm,status) "
                + "VALUES (%s,%s,%s)", [i_user, req["nomor-identitas"].strip(), req["status"].strip()])
            elif req["jabatan"] == "S":
                cursor.execute("INSERT INTO STAF(id_narasumber,nik_staf,posisi) "
                + "VALUES (%s,%s,%s)", [i_user, req["nomor-identitas"].strip(), req["status"].strip()])
            elif req["jabatan"] == "D":
                cursor.execute("INSERT INTO DOSEN(id_narasumber,nik_dosen,jurusan) "
                + "VALUES (%s,%s,%s)", [i_user, req["nomor-identitas"].strip(), req["status"].strip()])
                
        return render(request, 'registrasi/halaman-registrasi-berhasil.html', response)

def cek_username_ada(username):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM PENGGUNA WHERE username = %s", [username])
        return cursor.fetchone() is not None

def cek_username_valid(username):
    return all([i.isalnum() or i == '.' for i in username])

def cek_password_valid(password):
    return len(password) >= 8

def cek_nomor_identitas_valid(no_id):
    return all([i.isdigit() for i in no_id])

def cek_email_valid(email):
    return re.match(r"[^@]+@[^@]+\.[^@]+", email)

def cek_tanggal_valid(tanggal):
    try:
        datetime.datetime.strptime(tanggal, "%Y-%m-%d")
        return True
    except ValueError as e:
        return False

def cek_setiap_key_ada(req):
    try:
        for i in elemen_form:
            elem = req[i]
                
    except KeyError as e:
        return e.args[0]
        
def cek_nomor_identitas_ada(nomor_identitas, jabatan):
    tabel, nomor = "", ""
    
    if jabatan.upper() == "STAF":
        tabel, nomor = "STAF", "nik_staf"
    elif jabatan.upper() == "DOSEN":
        tabel, nomor = "DOSEN", "nik_dosen"
    elif jabatan.upper() == "MAHASISWA":
        tabel, nomor = "MAHASISWA", "npm"
    else:
        return False
        
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM " + tabel + " WHERE " + nomor + " = %s", [nomor_identitas])
        return cursor.fetchone() is not None

@csrf_exempt
def cek_nomor_identitas_ada_ajax(request):
    if request.method == 'POST':
        nomor_identitas = request.POST['nomor_identitas']
        jabatan = kepanjangan_jabatan[request.POST['jabatan']]
        hasil = cek_nomor_identitas_ada(nomor_identitas, jabatan)
        return JsonResponse({'hasil': json.dumps(hasil)})

@csrf_exempt
def cek_username_ada_ajax(request):
    if request.method == 'POST':
        username = request.POST['username']
        hasil = cek_username_ada(username)
        return JsonResponse({'hasil': json.dumps(hasil)})
