from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('polling_berita',polling_berita, name='polling_berita'),
]