from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.hashers import *
from django.db import connection,transaction
from core.models import Polling

# Create your views here.
def index(request):
    response = {}
    if 'bmnc_user_login' in request.session.keys():
        response["masuk"] = True
        response["adalah_narasumber"] = request.session["adalah_narasumber"]
        if response["adalah_narasumber"]:
            return render(request, 'poling_berita.html', response)
        else:
            messages.error(request, "Maaf, anda bukan Narasumber")
            return HttpResponseRedirect(reverse("beranda:index"))
    else:
        return HttpResponseRedirect(reverse("login:index"))
        
def polling_berita(request):
    if(request.method == 'POST'):
        req = request.POST
        url = req["url"].strip()
        mulai = req["mulai"].strip()
        selesai = req["selesai"].strip()
        i_user = 0
        for i in Polling.objects.raw("SELECT id FROM POLLING"):
            i_user = i.id
        i_user += 1
        with connection.cursor() as cursor:
            cursor.execute("SELECT url FROM BERITA WHERE url = %s",[url]);
            if cursor.fetchone() is None:
                messages.error(request, "BERITA BELUM ADA DI BMNC")
                return HttpResponseRedirect(reverse("polling_berita:index"))
            cursor.execute("INSERT INTO POLLING(id,polling_start,polling_end,total_responden) "
                           + "VALUES (%s,%s,%s,%s)", [i_user,mulai,selesai,1]);
            cursor.execute("INSERT INTO POLLING_BERITA(id_polling,url_berita) "
                           + "VALUES (%s,%s)", [i_user,url]);
        return HttpResponseRedirect(reverse("beranda:index"))