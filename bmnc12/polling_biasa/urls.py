from django.urls import path
from .views import *

urlpatterns = [
    path('', index, name='index'),
    path('polling_biasa', polling_biasa,name='polling_biasa'),
]