from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.hashers import *
from django.db import connection,transaction
from core.models import Polling
        
def index(request):
    response = {}
    if 'bmnc_user_login' in request.session.keys():
        response["masuk"] = True
        response["adalah_narasumber"] = request.session["adalah_narasumber"]
        if response["adalah_narasumber"]:
            return render(request, 'polling_biasa/polling_biasa.html', response)
        else:
            messages.error(request, "Maaf, anda bukan Narasumber")
            return HttpResponseRedirect(reverse("beranda:index"))
    else:
        return HttpResponseRedirect(reverse("login:index"))

def polling_biasa(request):
    if(request.method == 'POST'):
        req = request.POST
        deskripsi = req["Deskripsi"].strip()
        mulai = req["Mulai"].strip()
        selesai = req["Selesai"].strip()
        url = "http://localhost:8000/polling-biasa/"
        i_user = 0
        for i in Polling.objects.raw("SELECT id FROM POLLING"):
            i_user = i.id
        i_user += 1
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO POLLING(id,polling_start,polling_end,total_responden) "
                           + "VALUES (%s,%s,%s,%s)", [i_user,mulai,selesai,1]);
            cursor.execute("INSERT INTO POLLING_BIASA(id_polling,url,deskripsi) "
                           + "VALUES (%s,%s,%s)", [i_user,url,deskripsi]);
        return HttpResponseRedirect(reverse("beranda:index"))