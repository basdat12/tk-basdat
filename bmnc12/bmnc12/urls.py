"""bmnc12 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.conf.urls import url
import beranda.urls as beranda

urlpatterns = [
    path('login/', include(('login.urls', 'login'), namespace="login")),
    path('registrasi/', include(('registrasi.urls', 'registrasi'), namespace="registrasi")),
    path('buat-berita/', include(('buat_berita.urls', 'buat_berita'), namespace="buat_berita")),
    path('polling-biasa/', include(('polling_biasa.urls', 'polling_biasa'), namespace="polling_biasa")),
    path('polling-berita/', include(('polling_berita.urls', 'polling_berita'), namespace="polling_berita")),
    path('personal-profile/', include(('personal_profile.urls', 'personal_profile'), namespace="personal_profile")),
    path('lihat-polling/', include(('lihat_polling.urls', 'lihat_polling'), namespace="lihat_polling")),
    path('', include(('beranda.urls', 'beranda'), namespace="")),
]
