--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.13
-- Dumped by pg_dump version 9.5.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: bmnc; Type: SCHEMA; Schema: -; Owner: db032
--

CREATE SCHEMA bmnc;


ALTER SCHEMA bmnc OWNER TO db032;

--
-- Name: buat_kupon(); Type: FUNCTION; Schema: bmnc; Owner: db032
--

CREATE FUNCTION bmnc.buat_kupon() RETURNS trigger
    LANGUAGE plpgsql
    AS $$ 
  DECLARE 
    jumlah_kini   INTEGER; 
    id_kupon      INTEGER; 
    id_narasumber INTEGER; 
  BEGIN 
    SELECT jumlah_berita, 
           id 
    INTO   jumlah_kini, 
           id_narasumber 
    FROM   narasumber 
    WHERE  NEW.id_narasumber = narasumber.id; 
     
    IF (tg_op = 'INSERT') THEN 
      jumlah_kini = jumlah_kini + 1; 
    ELSIF (tg_op = 'DELETE') THEN 
      jumlah_kini = jumlah_kini - 1;
    END IF;
    
    UPDATE narasumber 
    SET    jumlah_berita = jumlah_kini 
    WHERE  NEW.id_narasumber = narasumber.id; 
     
    IF (tg_op = 'INSERT') THEN 
      IF (jumlah_kini%10 = 0) THEN 
        SELECT count(*) 
        INTO   id_kupon 
        FROM   kupon; 
         
        /* Kalau tabelnya berisi, akan 
           lebih aman kalau kita buat id baru 
           dari id terakhir + 1 */ 
        IF (id_kupon <> 0) THEN 
          SELECT   id 
          INTO     id_kupon 
          FROM     kupon 
          ORDER BY id DESC limit 1; 
        END IF;
        
        id_kupon = id_kupon + 1; 
        INSERT INTO kupon 
                    ( 
                                id, 
                                tgl_diberikan, 
                                tgl_kadaluarsa, 
                                id_narasumber 
                    ) 
                    VALUES 
                    ( 
                                id_kupon, 
                                current_timestamp, 
                                current_timestamp + interval '100 days', 
                                id_narasumber 
                    ); 
        END IF;
      RETURN NEW;
    ELSIF (tg_op = 'DELETE') THEN 
      RETURN OLD;
    END IF;
  END;
  $$;


ALTER FUNCTION bmnc.buat_kupon() OWNER TO db032;

--
-- Name: hitung(); Type: FUNCTION; Schema: bmnc; Owner: db032
--

CREATE FUNCTION bmnc.hitung() RETURNS void
    LANGUAGE plpgsql
    AS $$
 DECLARE
 row RECORD;
 
 BEGIN
 FOR row IN
 SELECT H.id_narasumber as id, COUNT(N.*) AS jumlah_berita
 FROM NARASUMBER N, HONOR H
 WHERE H.id_narasumber = N.id
 GROUP BY H.id_narasumber
 LOOP
UPDATE HONOR SET jumlah_berita = row.jumlah_berita
 WHERE id_narasumber = row.id;
 END LOOP;
 END;
$$;


ALTER FUNCTION bmnc.hitung() OWNER TO db032;

--
-- Name: password_minimal_8(); Type: FUNCTION; Schema: bmnc; Owner: db032
--

CREATE FUNCTION bmnc.password_minimal_8() RETURNS trigger
    LANGUAGE plpgsql
    AS $$  BEGIN    IF (tg_op = 'INSERT') THEN       IF (char_length(NEW.password) < 8) THEN        RAISE EXCEPTION 'Password minimal 8 karakter!';      END IF;      RETURN NEW;    END IF;  END;  $$;


ALTER FUNCTION bmnc.password_minimal_8() OWNER TO db032;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: berita; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.berita (
    url character varying(50) NOT NULL,
    judul character varying(100) NOT NULL,
    topik character varying(100) NOT NULL,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    jumlah_kata integer NOT NULL,
    rerata_rating double precision NOT NULL,
    id_universitas integer NOT NULL
);


ALTER TABLE bmnc.berita OWNER TO db032;

--
-- Name: dosen; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.dosen (
    id_narasumber integer NOT NULL,
    nik_dosen character varying(20) NOT NULL,
    jurusan character varying(20) NOT NULL
);


ALTER TABLE bmnc.dosen OWNER TO db032;

--
-- Name: honor; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.honor (
    id_narasumber integer NOT NULL,
    tgl_diberikan timestamp without time zone NOT NULL,
    jumlah_berita integer NOT NULL,
    jumlah_gaji integer NOT NULL
);


ALTER TABLE bmnc.honor OWNER TO db032;

--
-- Name: komentar; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.komentar (
    id integer NOT NULL,
    tanggal timestamp without time zone NOT NULL,
    jam time without time zone NOT NULL,
    konten character varying(100) NOT NULL,
    nama_user character varying(50) NOT NULL,
    email_user character varying(50) NOT NULL,
    url_user character varying(50) NOT NULL,
    url_berita character varying(50) NOT NULL
);


ALTER TABLE bmnc.komentar OWNER TO db032;

--
-- Name: kupon; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.kupon (
    id integer NOT NULL,
    tgl_diberikan timestamp without time zone NOT NULL,
    tgl_kadaluarsa timestamp without time zone NOT NULL,
    id_narasumber integer NOT NULL
);


ALTER TABLE bmnc.kupon OWNER TO db032;

--
-- Name: mahasiswa; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.mahasiswa (
    id_narasumber integer NOT NULL,
    npm character varying(20) NOT NULL,
    status character varying(20) NOT NULL
);


ALTER TABLE bmnc.mahasiswa OWNER TO db032;

--
-- Name: narasumber; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.narasumber (
    id integer NOT NULL,
    nama character varying(50) NOT NULL,
    email character varying(50) NOT NULL,
    tempat character varying(50) NOT NULL,
    tanggal character varying(50) NOT NULL,
    no_hp character varying(50) NOT NULL,
    jumlah_berita integer NOT NULL,
    rerata_kata integer NOT NULL,
    id_universitas integer NOT NULL
);


ALTER TABLE bmnc.narasumber OWNER TO db032;

--
-- Name: narasumber_berita; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.narasumber_berita (
    id_narasumber integer NOT NULL,
    url_berita character varying(50) NOT NULL
);


ALTER TABLE bmnc.narasumber_berita OWNER TO db032;

--
-- Name: pengguna; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.pengguna (
    id integer NOT NULL,
    username character varying(100) NOT NULL,
    role character varying(20) NOT NULL,
    password character varying(100) NOT NULL
);


ALTER TABLE bmnc.pengguna OWNER TO db032;

--
-- Name: polling; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.polling (
    id integer NOT NULL,
    polling_start timestamp without time zone NOT NULL,
    polling_end timestamp without time zone NOT NULL,
    total_responden integer NOT NULL
);


ALTER TABLE bmnc.polling OWNER TO db032;

--
-- Name: polling_berita; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.polling_berita (
    id_polling integer NOT NULL,
    url_berita character varying(50) NOT NULL
);


ALTER TABLE bmnc.polling_berita OWNER TO db032;

--
-- Name: polling_biasa; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.polling_biasa (
    id_polling integer NOT NULL,
    url character varying(50) NOT NULL,
    deskripsi character varying(100) NOT NULL
);


ALTER TABLE bmnc.polling_biasa OWNER TO db032;

--
-- Name: rating; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.rating (
    url_berita character varying(50) NOT NULL,
    ip_address character varying(50) NOT NULL,
    nilai double precision NOT NULL
);


ALTER TABLE bmnc.rating OWNER TO db032;

--
-- Name: rekening; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.rekening (
    nomor character varying(20) NOT NULL,
    nama_bank character varying(20) NOT NULL,
    id_narasumber integer NOT NULL
);


ALTER TABLE bmnc.rekening OWNER TO db032;

--
-- Name: respon; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.respon (
    id_polling integer NOT NULL,
    jawaban character varying(50) NOT NULL,
    jumlah_dipilih integer NOT NULL
);


ALTER TABLE bmnc.respon OWNER TO db032;

--
-- Name: responden; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.responden (
    id_polling integer NOT NULL,
    ip_address character varying(50) NOT NULL
);


ALTER TABLE bmnc.responden OWNER TO db032;

--
-- Name: riwayat; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.riwayat (
    url_berita character varying(50) NOT NULL,
    id_riwayat integer NOT NULL,
    waktu_revisi timestamp without time zone NOT NULL,
    konten text NOT NULL
);


ALTER TABLE bmnc.riwayat OWNER TO db032;

--
-- Name: staf; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.staf (
    id_narasumber integer NOT NULL,
    nik_staf character varying(20) NOT NULL,
    posisi character varying(20) NOT NULL
);


ALTER TABLE bmnc.staf OWNER TO db032;

--
-- Name: tag; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.tag (
    url_berita character varying(50) NOT NULL,
    tag character varying(50) NOT NULL
);


ALTER TABLE bmnc.tag OWNER TO db032;

--
-- Name: universitas; Type: TABLE; Schema: bmnc; Owner: db032
--

CREATE TABLE bmnc.universitas (
    id integer NOT NULL,
    jalan character varying(100) NOT NULL,
    kelurahan character varying(50) NOT NULL,
    provinsi character varying(50) NOT NULL,
    kodepos character varying(10) NOT NULL,
    website character varying(50) NOT NULL
);


ALTER TABLE bmnc.universitas OWNER TO db032;

--
-- Data for Name: berita; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.berita (url, judul, topik, created_at, updated_at, jumlah_kata, rerata_rating, id_universitas) FROM stdin;
\.


--
-- Data for Name: dosen; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.dosen (id_narasumber, nik_dosen, jurusan) FROM stdin;
\.


--
-- Data for Name: honor; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.honor (id_narasumber, tgl_diberikan, jumlah_berita, jumlah_gaji) FROM stdin;
\.


--
-- Data for Name: komentar; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.komentar (id, tanggal, jam, konten, nama_user, email_user, url_user, url_berita) FROM stdin;
\.


--
-- Data for Name: kupon; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.kupon (id, tgl_diberikan, tgl_kadaluarsa, id_narasumber) FROM stdin;
\.


--
-- Data for Name: mahasiswa; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.mahasiswa (id_narasumber, npm, status) FROM stdin;
\.


--
-- Data for Name: narasumber; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.narasumber (id, nama, email, tempat, tanggal, no_hp, jumlah_berita, rerata_kata, id_universitas) FROM stdin;
\.


--
-- Data for Name: narasumber_berita; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.narasumber_berita (id_narasumber, url_berita) FROM stdin;
\.


--
-- Data for Name: pengguna; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.pengguna (id, username, role, password) FROM stdin;
\.


--
-- Data for Name: polling; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.polling (id, polling_start, polling_end, total_responden) FROM stdin;
\.


--
-- Data for Name: polling_berita; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.polling_berita (id_polling, url_berita) FROM stdin;
\.


--
-- Data for Name: polling_biasa; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.polling_biasa (id_polling, url, deskripsi) FROM stdin;
\.


--
-- Data for Name: rating; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.rating (url_berita, ip_address, nilai) FROM stdin;
\.


--
-- Data for Name: rekening; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.rekening (nomor, nama_bank, id_narasumber) FROM stdin;
\.


--
-- Data for Name: respon; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.respon (id_polling, jawaban, jumlah_dipilih) FROM stdin;
\.


--
-- Data for Name: responden; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.responden (id_polling, ip_address) FROM stdin;
\.


--
-- Data for Name: riwayat; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.riwayat (url_berita, id_riwayat, waktu_revisi, konten) FROM stdin;
\.


--
-- Data for Name: staf; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.staf (id_narasumber, nik_staf, posisi) FROM stdin;
\.


--
-- Data for Name: tag; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.tag (url_berita, tag) FROM stdin;
\.


--
-- Data for Name: universitas; Type: TABLE DATA; Schema: bmnc; Owner: db032
--

COPY bmnc.universitas (id, jalan, kelurahan, provinsi, kodepos, website) FROM stdin;
1	Mangga Besar Raya (Sebagian)	Pangatikan	Jawa Barat	15247	https://univman-ja-vel.jw.ac.id
2	Arjuna Selatan	Johar Baru	Dki Jakarta	14961	https://univarj-dk-praesent.di.ac.id
3	Gunung Sahari 5	Batu Putih	Sulawesi Tenggara	15506	https://univgun-su-dapibus.sl.ac.id
4	Kesehatan Raya	Baki	Jawa Tengah	15679	https://univkes-ja-lacus.jw.ac.id
5	Kebayoran Baru	Tanimbar Utara	Maluku	15393	https://univkeb-ma-erat.ml.ac.id
6	Otto Iskandar Dinata	Rampi	Sulawesi Selatan	14927	https://univott-su-congue.sl.ac.id
7	Kebayoran Lama Raya (Sebagian)	Kelumbayan Barat	Lampung	15389	https://univkeb-la-quam.lm.ac.id
8	Kebon Jeruk	Penengahan	Lampung	15419	https://univkeb-la-convallis.lm.ac.id
9	Tipar Cakung (Sebagian)	Sungai Liat	Kepulauan Bangka Belitung	14430	https://univtip-ke-molestie.kp.ac.id
10	Arjuna Selatan	Gunung Pati	Jawa Tengah	14891	https://univarj-ja-dictumst.jw.ac.id
11	Medan Merdeka Utara	Geselma	Papua	15158	https://univmed-pa-ligula.pp.ac.id
12	Surya Wiyaja	Batang Gansal	Riau	15088	https://univsur-ri-magna.ra.ac.id
13	Bintaro Taman Barat	Bulagi Utara	Sulawesi Tengah	14999	https://univbin-su-cursus.sl.ac.id
14	Pluit Sakti	Kepulauan Seribu Selatan	Dki Jakarta	15180	https://univplu-dk-in.di.ac.id
15	Kapuk Raya	Mamberamo Ilir	Papua	15673	https://univkap-pa-etiam.pp.ac.id
16	Pagelarang	Pedes	Jawa Barat	15100	https://univpag-ja-turpis.jw.ac.id
17	Kemang 1	Miyah Selatan	Papua Barat	14817	https://univkem-pa-id.pp.ac.id
18	Tambak	Tegallalang	Bali	14969	https://univtam-ba-tristique.bl.ac.id
19	Munjul Raya	Kendari Barat	Sulawesi Tenggara	15366	https://univmun-su-ultrices.sl.ac.id
20	Bandengan Utara (Sebagian)	Wamena	Papua	14666	https://univban-pa-praesent.pp.ac.id
21	Mangga Besar Raya (Sebagian)	Sumobito	Jawa Timur	14938	https://univman-ja-proin.jw.ac.id
22	Pesangrahan	Medan Area	Sumatera Utara	14628	https://univpes-su-ac.sm.ac.id
23	Suryo Pranoto	Batu Ampar	Kepulauan Riau	14782	https://univsur-ke-vivamus.kp.ac.id
24	Utan Panjang Barat	Yamoneri	Papua	15688	https://univuta-pa-phasellus.pp.ac.id
25	Landasan Pacu Barat / Hbr. Motik	Sumalata	Gorontalo	14896	https://univlan-go-luctus.gr.ac.id
26	Garuda	Morosi	Sulawesi Tenggara	15639	https://univgar-su-a.sl.ac.id
27	Sunter Jaya (Ex. Sunter Kemayoran)	Sanga-Sanga	Kalimantan Timur	15625	https://univsun-ka-odio.kl.ac.id
28	Pos Pengumben Raya	Puring	Jawa Tengah	14102	https://univpos-ja-odio.jw.ac.id
29	Pondok Gede Raya	Mijen	Jawa Tengah	14084	https://univpon-ja-integer.jw.ac.id
30	Rs. Fatmawati	Jetis	Di Yogyakarta	15046	https://univrs.-di-bibendum.dy.ac.id
31	Lapangan Bola	Biring Kanaya	Sulawesi Selatan	15563	https://univlap-su-vel.sl.ac.id
32	Imam Bonjol	Panca Rijang	Sulawesi Selatan	15384	https://univima-su-tortor.sl.ac.id
33	Danau Sunter Selatan	Pademangan	Dki Jakarta	14080	https://univdan-dk-sem.di.ac.id
34	KH. Wahid Hasyim	Dokome	Papua	15951	https://univkh.-pa-elementum.pp.ac.id
35	Rorotan 5	Mamberamo Ilir	Papua	14555	https://univror-pa-lacinia.pp.ac.id
36	Mabes Polri / Kavling Polri	Padang Laweh	Sumatera Barat	15197	https://univmab-su-vel.sm.ac.id
37	Kali Besar Timur 3	Kwelamdua	Papua	15463	https://univkal-pa-leo.pp.ac.id
38	Kelapa Dua Wetan	Umagi	Papua	15166	https://univkel-pa-adipiscing.pp.ac.id
39	Perwira	Karanganyar	Jawa Tengah	15213	https://univper-ja-nibh.jw.ac.id
40	Paso	Ranca Bungur	Jawa Barat	14996	https://univpas-ja-congue.jw.ac.id
41	Medan Merdeka Barat	Krayan Timur	Kalimantan Utara	14947	https://univmed-ka-eu.kl.ac.id
42	Cilangkap Baru	Kepahiang	Bengkulu	14578	https://univcil-be-dui.bn.ac.id
43	Sentra Primer Timur / Dr. Sumarno	Maleber	Jawa Barat	15729	https://univsen-ja-dapibus.jw.ac.id
44	Danau Sunter Barat	Purwosari	Di Yogyakarta	14893	https://univdan-di-non.dy.ac.id
45	Kopur/Ra. Fadillah	Wonomulyo	Sulawesi Barat	16312	https://univkop-su-malesuada.sl.ac.id
46	Kapuk Raya	Putussibau Selatan	Kalimantan Barat	14907	https://univkap-ka-quisque.kl.ac.id
47	Pramuka Raya	Sukahening	Jawa Barat	14562	https://univpra-ja-quis.jw.ac.id
48	Jembatan Batu	Karang Jaya	Sumatera Selatan	15713	https://univjem-su-in.sm.ac.id
49	Tambak	Torgamba	Sumatera Utara	15692	https://univtam-su-condimentum.sm.ac.id
50	Cipayung Baru Raya	Sewon	Di Yogyakarta	15318	https://univcip-di-et.dy.ac.id
51	Boulevard Artha Gading	Sedati	Jawa Timur	13887	https://univbou-ja-ullamcorper.jw.ac.id
52	Gunung Sahari 5	Tomu	Papua Barat	14513	https://univgun-pa-amet.pp.ac.id
53	Taman Bunga	Sukaresmi	Banten	15773	https://univtam-ba-luctus.bn.ac.id
54	Sumur Bor	Kotabunan	Sulawesi Utara	15719	https://univsum-su-euismod.sl.ac.id
55	Cilangkap Baru	Kuantan Mudik	Riau	15446	https://univcil-ri-sociis.ra.ac.id
56	Gunung Sahari Raya (Sebagian)	Kota Kendal	Jawa Tengah	14549	https://univgun-ja-hac.jw.ac.id
57	Pos	Torere	Papua	13950	https://univpos-pa-in.pp.ac.id
58	HR. Rasuna Said	Kwelamdua	Papua	15516	https://univhr.-pa-sit.pp.ac.id
59	Dr. Suratmo	Cikarang Utara	Jawa Barat	15361	https://univdr.-ja-eu.jw.ac.id
60	Utan Jati (Peta Timur)	Sukahening	Jawa Barat	15709	https://univuta-ja-vestibulum.jw.ac.id
61	Matraman Raya	Ranca Bungur	Jawa Barat	14004	https://univmat-ja-ut.jw.ac.id
62	Jamblang	Sukahening	Jawa Barat	15778	https://univjam-ja-dui.jw.ac.id
63	Pantai Indah Barat	Putra Rumbia	Lampung	15124	https://univpan-la-sed.lm.ac.id
64	Pluit Barat Raya	Batang Gansal	Riau	15014	https://univplu-ri-non.ra.ac.id
65	Cipayung Raya	Umagi	Papua	14586	https://univcip-pa-neque.pp.ac.id
66	Taman Bunga	Sumalata	Gorontalo	14114	https://univtam-go-vel.gr.ac.id
67	Jelambar Timur	Benteng	Sulawesi Selatan	15207	https://univjel-su-eleifend.sl.ac.id
68	Mangga Besar Raya (Sebagian)	Ketambe	Aceh	15029	https://univman-ac-neque.ae.ac.id
69	TMII 1	Sukasada	Bali	14337	https://univtmi-ba-erat.bl.ac.id
70	Sentra Primer Timur / Dr. Sumarno	Mutis	Nusa Tenggara Timur	14994	https://univsen-nu-pellentesque.ns.ac.id
71	Tongkol	Torere	Papua	15480	https://univton-pa-nisl.pp.ac.id
72	Warung Gantung	Kumpeh	Jambi	15178	https://univwar-ja-habitasse.jm.ac.id
73	Pluit Barat Raya	Semarang Barat	Jawa Tengah	15247	https://univplu-ja-sollicitudin.jw.ac.id
74	Landasan Pacu Barat / Hbr. Motik	Slahung	Jawa Timur	13947	https://univlan-ja-nisi.jw.ac.id
75	Prapatan	Manggar	Kepulauan Bangka Belitung	15417	https://univpra-ke-nisl.kp.ac.id
76	Taman Bunga	Mamosalato	Sulawesi Tengah	15257	https://univtam-su-integer.sl.ac.id
77	Kebayoran Lama Raya (Sebagian)	Taliabu Barat Laut	Maluku Utara	14898	https://univkeb-ma-lectus.ml.ac.id
78	Pahlawan Kali Nata (Sebagian)	Morosi	Sulawesi Tenggara	15043	https://univpah-su-justo.sl.ac.id
79	TMII 1	Kabaena	Sulawesi Tenggara	14520	https://univtmi-su-et.sl.ac.id
80	Danau Sunter Selatan	Mandah	Riau	14114	https://univdan-ri-posuere.ra.ac.id
81	Bina Marga	Gunung Pati	Jawa Tengah	15018	https://univbin-ja-tortor.jw.ac.id
82	Kayu Putih	Gadung	Sulawesi Tengah	14885	https://univkay-su-turpis.sl.ac.id
83	Komodor Halim PK. (Ex.Venus)	Indihiang	Jawa Barat	14915	https://univkom-ja-donec.jw.ac.id
84	KH. Wahid Hasyim	Weime	Papua	15716	https://univkh.-pa-dignissim.pp.ac.id
85	Rorotan 5	Kuok	Riau	13865	https://univror-ri-justo.ra.ac.id
86	Bandengan Utara (Sebagian)	Kelumbayan Barat	Lampung	15524	https://univban-la-felis.lm.ac.id
87	Medan Merdeka Selatan	Kotawaringin Lama	Kalimantan Tengah	15481	https://univmed-ka-id.kl.ac.id
88	Tanjung Barat	Putra Rumbia	Lampung	14842	https://univtan-la-sed.lm.ac.id
89	Bintaro Taman Barat	Gunung Pati	Jawa Tengah	15851	https://univbin-ja-elementum.jw.ac.id
90	Kemang Raya	Gunung Pati	Jawa Tengah	15286	https://univkem-ja-lobortis.jw.ac.id
91	Mitra Sunter Boulevard	Montasik	Aceh	13769	https://univmit-ac-in.ae.ac.id
92	Pondok Kopi Raya	Batang Gansal	Riau	15343	https://univpon-ri-vel.ra.ac.id
93	Cilandak Raya	Maleber	Jawa Barat	14800	https://univcil-ja-libero.jw.ac.id
94	Kapuk Kamal Raya	Pinang	Banten	15743	https://univkap-ba-ultrices.bn.ac.id
95	Matraman Dalam	Abenaho	Papua	14720	https://univmat-pa-nibh.pp.ac.id
96	Puri Lingkar Luar	Trumon	Aceh	15639	https://univpur-ac-in.ae.ac.id
97	Utan Jati (Peta Timur)	Atu Lintang	Aceh	15241	https://univuta-ac-adipiscing.ae.ac.id
98	Kampung Bandan	Abun	Papua Barat	15053	https://univkam-pa-ligula.pp.ac.id
99	Cipayung Baru Raya	Pulau Laut Utara	Kalimantan Selatan	15483	https://univcip-ka-congue.kl.ac.id
100	Pintu Besar Selatan	Seberang Ulu I	Sumatera Selatan	15676	https://univpin-su-sapien.sm.ac.id
\.


--
-- Name: berita_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.berita
    ADD CONSTRAINT berita_pkey PRIMARY KEY (url);


--
-- Name: dosen_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.dosen
    ADD CONSTRAINT dosen_pkey PRIMARY KEY (id_narasumber);


--
-- Name: honor_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.honor
    ADD CONSTRAINT honor_pkey PRIMARY KEY (tgl_diberikan, id_narasumber);


--
-- Name: komentar_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.komentar
    ADD CONSTRAINT komentar_pkey PRIMARY KEY (id);


--
-- Name: kupon_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.kupon
    ADD CONSTRAINT kupon_pkey PRIMARY KEY (id);


--
-- Name: mahasiswa_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.mahasiswa
    ADD CONSTRAINT mahasiswa_pkey PRIMARY KEY (id_narasumber);


--
-- Name: narasumber_berita_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.narasumber_berita
    ADD CONSTRAINT narasumber_berita_pkey PRIMARY KEY (url_berita, id_narasumber);


--
-- Name: narasumber_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.narasumber
    ADD CONSTRAINT narasumber_pkey PRIMARY KEY (id);


--
-- Name: pengguna_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.pengguna
    ADD CONSTRAINT pengguna_pkey PRIMARY KEY (id);


--
-- Name: pengguna_username_key; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.pengguna
    ADD CONSTRAINT pengguna_username_key UNIQUE (username);


--
-- Name: poling_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.polling
    ADD CONSTRAINT poling_pkey PRIMARY KEY (id);


--
-- Name: rating_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.rating
    ADD CONSTRAINT rating_pkey PRIMARY KEY (url_berita, ip_address);


--
-- Name: rekening_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.rekening
    ADD CONSTRAINT rekening_pkey PRIMARY KEY (nomor);


--
-- Name: respon_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.respon
    ADD CONSTRAINT respon_pkey PRIMARY KEY (id_polling, jawaban);


--
-- Name: responden_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.responden
    ADD CONSTRAINT responden_pkey PRIMARY KEY (id_polling, ip_address);


--
-- Name: riwayat_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.riwayat
    ADD CONSTRAINT riwayat_pkey PRIMARY KEY (url_berita, id_riwayat);


--
-- Name: staf_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.staf
    ADD CONSTRAINT staf_pkey PRIMARY KEY (id_narasumber);


--
-- Name: tag_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.tag
    ADD CONSTRAINT tag_pkey PRIMARY KEY (url_berita, tag);


--
-- Name: universitas_pkey; Type: CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.universitas
    ADD CONSTRAINT universitas_pkey PRIMARY KEY (id);


--
-- Name: buat_kupon_untuk_narasumber; Type: TRIGGER; Schema: bmnc; Owner: db032
--

CREATE TRIGGER buat_kupon_untuk_narasumber BEFORE INSERT ON bmnc.narasumber_berita FOR EACH ROW EXECUTE PROCEDURE bmnc.buat_kupon();


--
-- Name: panjang_minimal_password; Type: TRIGGER; Schema: bmnc; Owner: db032
--

CREATE TRIGGER panjang_minimal_password BEFORE INSERT ON bmnc.pengguna FOR EACH ROW EXECUTE PROCEDURE bmnc.password_minimal_8();


--
-- Name: berita_id_universitas_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.berita
    ADD CONSTRAINT berita_id_universitas_fkey FOREIGN KEY (id_universitas) REFERENCES bmnc.universitas(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: dosen_id_narasumber_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.dosen
    ADD CONSTRAINT dosen_id_narasumber_fkey FOREIGN KEY (id_narasumber) REFERENCES bmnc.narasumber(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: honor_id_narasumber_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.honor
    ADD CONSTRAINT honor_id_narasumber_fkey FOREIGN KEY (id_narasumber) REFERENCES bmnc.narasumber(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: komentar_url_berita_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.komentar
    ADD CONSTRAINT komentar_url_berita_fkey FOREIGN KEY (url_berita) REFERENCES bmnc.berita(url) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: kupon_id_narasumber_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.kupon
    ADD CONSTRAINT kupon_id_narasumber_fkey FOREIGN KEY (id_narasumber) REFERENCES bmnc.narasumber(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: mahasiswa_id_narasumber_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.mahasiswa
    ADD CONSTRAINT mahasiswa_id_narasumber_fkey FOREIGN KEY (id_narasumber) REFERENCES bmnc.narasumber(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: narasumber_berita_id_narasumber_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.narasumber_berita
    ADD CONSTRAINT narasumber_berita_id_narasumber_fkey FOREIGN KEY (id_narasumber) REFERENCES bmnc.narasumber(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: narasumber_berita_url_berita_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.narasumber_berita
    ADD CONSTRAINT narasumber_berita_url_berita_fkey FOREIGN KEY (url_berita) REFERENCES bmnc.berita(url) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: narasumber_id_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.narasumber
    ADD CONSTRAINT narasumber_id_fkey FOREIGN KEY (id_universitas) REFERENCES bmnc.universitas(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: narasumber_narasumber_id_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.narasumber
    ADD CONSTRAINT narasumber_narasumber_id_fkey FOREIGN KEY (id) REFERENCES bmnc.pengguna(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: poling_berita_id_polling_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.polling_berita
    ADD CONSTRAINT poling_berita_id_polling_fkey FOREIGN KEY (id_polling) REFERENCES bmnc.polling(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: poling_biasa_id_polling_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.polling_biasa
    ADD CONSTRAINT poling_biasa_id_polling_fkey FOREIGN KEY (id_polling) REFERENCES bmnc.polling(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rating_url_berita_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.rating
    ADD CONSTRAINT rating_url_berita_fkey FOREIGN KEY (url_berita) REFERENCES bmnc.berita(url) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: rekening_id_narasumber_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.rekening
    ADD CONSTRAINT rekening_id_narasumber_fkey FOREIGN KEY (id_narasumber) REFERENCES bmnc.narasumber(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: respon_id_polling_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.respon
    ADD CONSTRAINT respon_id_polling_fkey FOREIGN KEY (id_polling) REFERENCES bmnc.polling(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: responden_id_polling_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.responden
    ADD CONSTRAINT responden_id_polling_fkey FOREIGN KEY (id_polling) REFERENCES bmnc.polling(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: riwayat_url_berita_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.riwayat
    ADD CONSTRAINT riwayat_url_berita_fkey FOREIGN KEY (url_berita) REFERENCES bmnc.berita(url) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: staf_id_narasumber_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.staf
    ADD CONSTRAINT staf_id_narasumber_fkey FOREIGN KEY (id_narasumber) REFERENCES bmnc.narasumber(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: tag_url_berita_fkey; Type: FK CONSTRAINT; Schema: bmnc; Owner: db032
--

ALTER TABLE ONLY bmnc.tag
    ADD CONSTRAINT tag_url_berita_fkey FOREIGN KEY (url_berita) REFERENCES bmnc.berita(url) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

